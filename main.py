import numpy as np
from PIL import ImageGrab
import cv2
import time

def create_cv_greyCopy(img):
    cv_fingerPrint = img.astype(np.uint8)
    cv_fingerPrintGrey = cv2.cvtColor(cv_fingerPrint, cv2.COLOR_RGB2GRAY)
    cv_fingerPrintGreyCopy = cv_fingerPrintGrey.copy()
    # cv2.imshow('window', cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    return cv_fingerPrintGreyCopy


def create_search_array():
    output = []
    #column, row
    #0,0
    cv_print1 = create_cv_greyCopy(np.array(ImageGrab.grab(bbox=(478,273,478+112,273+112))))
    cv_print1 = cv2.resize(cv_print1,(150,150))
    output.append(cv_print1)

    #0,1
    cv_print2 = create_cv_greyCopy(np.array(ImageGrab.grab(bbox=(478, 416, 478+112, 416+112))))
    cv_print2 = cv2.resize(cv_print2,(150,150))
    output.append(cv_print2)

    # 0,2
    cv_print3 = create_cv_greyCopy(np.array(ImageGrab.grab(bbox=(478, 562, 478+112, 562+112))))
    cv_print3 = cv2.resize(cv_print3,(150,150))
    output.append(cv_print3)

    # 0,3
    cv_print4 = create_cv_greyCopy(np.array(ImageGrab.grab(bbox=(478, 706, 478+112, 706+112))))
    cv_print4 = cv2.resize(cv_print4,(150,150))
    output.append(cv_print4)

    #1,0
    cv_print5 = create_cv_greyCopy(np.array(ImageGrab.grab(bbox=(622,273,622+112,273+112))))
    cv_print5 = cv2.resize(cv_print5,(150,150))
    output.append(cv_print5)

    #1,1
    cv_print6 = create_cv_greyCopy(np.array(ImageGrab.grab(bbox=(622, 416, 622+112, 416+112))))
    cv_print6 = cv2.resize(cv_print6,(150,150))
    output.append(cv_print6)

    # 1,2
    cv_print7 = create_cv_greyCopy(np.array(ImageGrab.grab(bbox=(622, 562, 622+112, 562+112))))
    cv_print7 = cv2.resize(cv_print7,(150,150))
    output.append(cv_print7)

    # 1,3
    cv_print8 = create_cv_greyCopy(np.array(ImageGrab.grab(bbox=(622, 706, 622+112, 706+112))))
    cv_print8 = cv2.resize(cv_print8,(150,150))
    output.append(cv_print8)

    return output


def screen_record():
    last_time = time.time()
    text_positions = [(30,50), (30, 195), (30, 340), (30, 485), (175,50), (175, 195), (175, 340), (175, 485)]
    while(True):
        # from 1185x464 to 1217x775 windowed mode
        fingerPrint =  np.array(ImageGrab.grab(bbox=(965,150,1315,670)))

        possibilities = np.array(ImageGrab.grab(bbox=(450, 250, 770, 830)))

        cv_fingerPrintGreyCopy = create_cv_greyCopy(fingerPrint)

        templates = create_search_array()
        img = cv_fingerPrintGreyCopy.copy()
        count = 0
        result = []

        for template in templates:

            #template = cv2.imread("TestPictures/search.png", 0)
            w, h = template.shape[::-1]

            # All the 6 methods for comparison in a list
            #methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
            #           'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']
            methods = ['cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR_NORMED']
            # methods = ['cv2.TM_CCOEFF_NORMED']
            max_val_sum = 0

            for meth in methods:
                method = eval(meth)
                # Apply template Matching
                res = cv2.matchTemplate(img, template, method)
                min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

                # save results
                # print("max_val: " + str(max_val) + " Count: " + str(count) +" Type: " + str(meth))
                max_val_sum = max_val_sum+max_val

            max_val_sum = max_val_sum / len(methods)
            result.append((count, max_val_sum))

            count = count + 1

            #print('loop took {} seconds'.format(time.time()-last_time))

        result.sort(key=lambda tup: tup[1], reverse=True)
        # print(result)
        for i in range(len(result)):
            if i < 4:
                possibilities = cv2.putText(possibilities, "{:.3f}".format(result[i][1]), text_positions[result[i][0]],
                                            cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
                                            1, (0, 128, 0), 2, cv2.LINE_AA)
            elif i == 4:
                possibilities = cv2.putText(possibilities, "{:.3f}".format(result[i][1]), text_positions[result[i][0]],
                                            cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
                                            1, (0, 165, 255), 2, cv2.LINE_AA)
            elif i == 5:
                possibilities = cv2.putText(possibilities, "{:.3f}".format(result[i][1]), text_positions[result[i][0]],
                                            cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
                                            1, (0, 0, 255), 2, cv2.LINE_AA)
            else:
                possibilities = cv2.putText(possibilities, "{:.3f}".format(result[i][1]), text_positions[result[i][0]],
                                            cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
                                            1, (255, 255, 255), 2, cv2.LINE_AA)

        last_time = time.time()
        cv2.imshow('possibilities ', possibilities)
        if cv2.waitKey(25) & 0xFF == ord('q'):
            cv2.destroyAllWindows()
            break

def main():
    screen_record()


if __name__ == "__main__":
    main()